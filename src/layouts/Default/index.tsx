import react, {useState} from 'react';
import * as S from "./style";
import { 
  Sidebar,
  TaskList,
  TaskDetail 
} from "components";

import Switch from "react-switch";

import { IoIosAdd, IoIosArrowDown } from "react-icons/io";
import { IoSearchOutline } from "react-icons/io5";
import { FaRegBell } from "react-icons/fa";
import { CgSwapVertical, CgMoreVerticalAlt } from "react-icons/cg";

import Avatar from "assets/images/avatar.jpg";

interface Props {
  className?: string;
  content?: any;
}

function Default(props: Props){
  const { content } = props
  const [openForm, setOpenForm] = useState(false)
  const [checked, setChecked] = useState(false)
  const [favorite, setFavorite] = useState(false)
  const [dataTaskList, setDataTaskList] = useState([])
  const handleOpenAddForm = () => {
    setOpenForm(true)
  }

  const handleCloseAddForm = () => {
    setOpenForm(false)
  }

  const submitAddForm = (e:any) => {
    e.preventDefault();
    console.log("data",e);
    setDataTaskList(e)
  }

  const handleChangeChecked = () => {
    setChecked(!checked);
  }

  const handleChangeFavorite = () => {
    setFavorite(!favorite);
  }

  return(
    <S.Wrapper>
      <Sidebar/>
      {/* { content } */}
      <S.Content>
        <S.TopBar>
          <S.SearchBar>
            <button className="search-btn">
              <IoSearchOutline/>
            </button>
            <input type="text" placeholder="Search..." name="search" className="search" />
          </S.SearchBar>
          <S.Notification>
            <FaRegBell className="notifi-icon"/>
            <span className="notifi-number"></span>
          </S.Notification>
          <S.UserInfo>
            <img src={Avatar} alt="avatar" className="avatar" />
            <span className="user-name">Thai Nguyen</span>
            <IoIosArrowDown className="dropdown-icon" />
          </S.UserInfo>
        </S.TopBar>
        <S.ContentWrapper>
          <S.ColumnTask>
            <div className="task-head">
              <span className="task-head-name">
                Design
              </span>
              <button className="task-head-btn">
                <CgSwapVertical/>
              </button>
              <button className="task-head-btn">
                <CgMoreVerticalAlt/>
              </button>
            </div>
            <TaskList data={dataTaskList} />
          </S.ColumnTask>
          <TaskDetail className="task-detail"/>
        </S.ContentWrapper>
      </S.Content>
      <S.ButtonAdd className="btn-add" onClick={handleOpenAddForm}>
        <IoIosAdd/>
      </S.ButtonAdd>
      {openForm === true &&
        <S.AddForm>
          <div className="form-title">
            <h4>Add your new task</h4>
          </div>
          <form className="form-wrapper" onSubmit={submitAddForm}>
            <div className="form-field">
              <span className="form-label">Task name:</span>
              <input className="name-input" type="text"/>
            </div>
            <div className="form-field swicth-group">
              <div className="swicth">
                <span className="form-label">Checked:</span>
                <Switch 
                  onColor="#2684ff" 
                  onChange={handleChangeFavorite} 
                  checked={favorite} 
                  handleDiameter={30}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                  activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                  height={20}
                  width={48}
                />
              </div>
              <div className="swicth">
                <span className="form-label">Mark as favorite:</span>
                <Switch 
                  onColor="#2684ff" 
                  onChange={handleChangeChecked} 
                  checked={checked} 
                  handleDiameter={30}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                  activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                  height={20}
                  width={48}
                />
              </div>
            </div>
            <div className="form-btn">
              <button type="submit" className="form-create-btn">
                Create
              </button>
              <button className="form-cancel-btn" onClick={handleCloseAddForm} >
                Cancel
              </button>
            </div>
          </form>
        </S.AddForm>
      }
    </S.Wrapper>
  );
}

export default Default;
import styled from "styled-components";

export const Wrapper = styled.div`
max-width: 1600px;
/* min-height: 850px; */
margin: auto;
backdrop-filter: blur(5px);
display: flex;
flex-wrap: wrap;
position: relative;
.sidebar{
  width: 260px;
  flex-shrink: 0;
  background-color: rgba(255, 255, 255, 0.8);
}
`;

export const Content = styled.div`
  width: calc(100% - 261px);
`;

export const ButtonAdd = styled.button`
  cursor: pointer;
  width: 50px;
  height: 50px;
  border-radius: 50px;
  background: #ff7553;
  border: none;
  color: #ffffff;
  font-size: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 2px 3px 10px rgb(255 117 83 / 15%);
  position: absolute;
  left: 50px;
  bottom: 50px;
`;

export const TopBar = styled.div`
  display: flex;
  align-items: center;
  height: 60px;
  background-color: rgba(255, 255, 255, 0.7);
  border-bottom: rgba(80, 95, 121, 0.2) 1px solid;
`;

export const ContentWrapper = styled.div`
  background-color: rgba(255, 255, 255, 0.8);
  width: 100%;
  height: calc(100% - 61px);
  display: flex;
  /* flex-wrap: wrap; */
  flex-flow: row wrap;
  .task-detail{
    /* max-width: calc(65% - 1px);
    flex: 0 0 calc(65% - 1px); */
    width: calc(65% - 1px);
    padding: 50px 0px;
  }
`;

export const SearchBar = styled.div`
  width: calc(100% - 327px);
  display: flex;
  justify-content: center;
  padding: 10px 30px 0;
  .search{
    width: 100%;
    height: 40px;
    border: none;
    font-size: 16px;
    font-weight: 500;
    color: #505f79;
    background: transparent;
    outline: none !important;
    &:-internal-autofill-selected{
      background: transparent !important;
    }
  }
  .search-btn{
    border: none;
    background: transparent;
    font-size: 18px;
    color: #505f79;
  }
`;

export const Notification = styled.div`
  width: 75px;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border-left: rgba(80, 95, 121, 0.2) 1px solid;
  border-right: rgba(80, 95, 121, 0.2) 1px solid;
  position: relative;
  .notifi-icon{
    font-size: 24px;
    color: #757575;
  }
  .notifi-number{
    width: 8px;
    height: 8px;
    border-radius: 50%;
    position: absolute;
    top: 17px;
    right: 24px;
    background: #ff7553;
    border: 1px rgba(255, 255, 255, 0.7) solid;
  }
`;

export const UserInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  min-width: 190px;
  padding: 0 30px;
  .avatar{
    border-radius: 5px;
    width: 36px;
    height: 36px;
  }
  .user-name{
    font-size: 16px;
    font-weight: 500;
    color: #505f79;
  }
  .dropdown-icon{
    font-size: 18px;
    color: #757575;
  }
`;

export const ColumnTask = styled.div`
  /* max-width: 35%;
  flex: 0 0 35%; */
  width: 35%;
  height: 100%;
  border-right: rgba(80, 95, 121, 0.2) 1px solid;
  .task-head{
    height: 70px;
    border-bottom: 5px #2684ff solid;
    display: flex;
    align-items: center;
  }
  .task-head-name{
    font-size: 16px;
    text-transform: uppercase;
    font-weight: 500;
    padding-top: 10px;
    padding-left: 30px;
    width: calc(100% - 120px);
  }
  .task-head-btn{
    font-size: 30px;
    border: none;
    background: transparent;
    padding: 10px 0 0;
    color: #757575;
    width: 60px;
    cursor: pointer;
  }
  .task-list{
    max-height: 714px;
    overflow-y: auto;
  }
`;

export const AddForm = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 400px;
  background: #ffffff;
  padding: 20px;
  .form-title h4{
    font-size: 16px;
    margin-top: 10px;
    text-align: center;
  }
  .form-field{
    margin-bottom: 15px;
    display: flex;
    flex-direction: column;
  }
  .form-label{
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 5px;
    margin-right: 10px;
  }
  input{
    max-width: 100%;
    border: 2px rgba(38, 132, 255, 0.3) solid;
    height: 36px;
    border-radius: 5px;
    outline: none !important;
    padding: 0 10px;
  }
  .swicth-group{
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: row;
  }
  .swicth{
    display: flex;
    align-items: center;
  }
  .form-btn{
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 30px 0 10px;
    .form-create-btn,
    .form-cancel-btn{
      border: none;
      height: 38px;
      line-height: 38px;
      padding: 0 20px;
      margin: 0 5px;
      cursor: pointer;
      border-radius: 5px;
      color: #ffffff;
      font-size: 14px;
    }
    .form-create-btn{
      background: #ff7553;
    }
    .form-cancel-btn{
      background: #2684ff;
    }
  }
`;

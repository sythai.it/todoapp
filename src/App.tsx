import React from 'react';
import styled from "styled-components";
import { Switch, Route, Redirect } from "react-router-dom";
import GlobalStyles from "./global-styles";

import Routes from "constants/routes";
import {PrivateRoute} from "components";
import {Default} from "layouts";

import Bg from "assets/images/background.jpg";

// page
import {
  Home
} from "./pages";

const AppWrapper = styled.div`
  background-image: url(${Bg});
  background-size: cover;
  background-position: center center;
  min-height: 100vh;
  padding-top: 30px;
`;

function App() {

  return (
    <AppWrapper>
      <Default></Default>
      <GlobalStyles />
    </AppWrapper>
  );
}

export default App;

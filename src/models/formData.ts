export interface WhitelistFormData {
  firstName: string
  lastName: string
  eventID: number
  email: string
  organization?: string
  jobTitle?: string
}
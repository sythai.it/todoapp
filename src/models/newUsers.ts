export interface NewUser {
  id: number
  firstName: string
  lastName: string
  country: string
  countryName: string
  jobFunction: string
  jobTitle: string
  businessType: string
  organization: string
  city: string
  address?: string | null
  postalCode?: number | null
  contactNumber?: string | null
  about?: string | null
  gender?: string | null
  userid: string
  imageUrl?: string
  createdAt?: string
  updatedAt?: string
  profiles: {
    email: string
    confirmed: number
    status: number
  }
  expandable: boolean
}
export interface NewUsers {
  msgCode?: string
  data: {
    newUsers: Array<NewUser>
    currentPage?: number
    totalPages?: number
    limit?: number
    count?: number
  }
  errors?: object | null
}
export interface ActionResponse {
  msgCode: string
  success: boolean
}

export interface NewUsersState {
  isLoading: boolean
  data: NewUsers
  msgCode?: string
  success?: boolean
}
import { NewUser } from './newUsers'

export interface Whitelist {
  msgCode?: string
  data: {
    whitelist: Array<NewUser>
    currentPage?: number
    totalPages?: number
    limit?: number
    count?: number
  }
  errors?: object | null
}

export interface ActionResponse {
  msgCode: string
  success: boolean
}

export interface WhitelistState {
  isLoading: boolean
  data: Whitelist
  msgCode?: string
  success?: boolean
}
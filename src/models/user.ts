export interface User {
  userid: string,
  accessToken: string,
  msgCode: string | null,
  role: string | null,
  email: string
}

export interface UserResult {
  success: boolean | null,
  data: User,
  errors: object | null
}
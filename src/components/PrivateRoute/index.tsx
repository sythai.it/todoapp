import React from 'react'
import {
  Route,
  Redirect,
  RouteProps,
} from 'react-router-dom'

import Routes from 'constants/routes'

interface PrivateRouteProps extends RouteProps {
  component: any;
  isLoggedIn: boolean;
}

const PrivateRoute = (props: PrivateRouteProps) => {
  const { component: Component, isLoggedIn, ...rest } = props

  return (
    <Route
      {...rest}
      render={(routeProps) =>
        isLoggedIn ? (
          <Component {...routeProps} />
        ) : (
          <Redirect
            to={{
              pathname: Routes.LOGIN,
              state: { from: routeProps.location }
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
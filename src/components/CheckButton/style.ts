import styled from "styled-components";

export const Wrapper = styled.div`
  .check-btn{
      width: 40px;
      height: 40px;
      border: 2px rgba(38, 132, 255, 0.3) solid;
      border-radius: 50%;
      background: transparent;
      cursor: pointer;
      &.active{
        border: 2px #2684ff solid;
        background: #2684ff;
        box-shadow: 2px 3px 10px rgb(38 132 255 / 15%);
      }
      .checked{
        font-size: 24px;
        color: #ffffff;
      }
    }
`;
import react, {useState} from 'react';
import { GoCheck } from "react-icons/go";

import * as S from "./style";

interface Props {
  className?: string;
  isChecked?: boolean;
  onClick?:()=> void;
}

function CheckButton(props: Props){
  const { className, isChecked, onClick } = props;
  // const [isDone, setDone] = useState(false);
  // const handleCheck = () => {
  //   setDone(!isChecked)
  // }

  return(
    <S.Wrapper className={className}>
      <button 
        className={`check-btn ${isChecked === true && 'active'}`} 
        onClick={onClick}
      >
        {isChecked === true && <GoCheck className="checked" />}
      </button>
    </S.Wrapper>
  );
}

export default CheckButton;
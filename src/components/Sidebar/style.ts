import styled from "styled-components";

export const Wrapper = styled.div`
  /* border-right: rgba(255, 255, 255, 0.2) 1px solid; */
  border-right: rgba(80, 95, 121, 0.2) 1px solid;
  /* border-right: #eaeaea 1px solid; */
`;

export const Logo = styled.div`
  padding: 26px 20px 18px 40px;
  display: flex;
  align-items: flex-start;
  .logo{
    font-size: 20px;
    color: #ff7553;
    margin-right: 15px;
  }
  .logo-name{
    text-transform: uppercase;
    font-weight: 400;
    font-size: 20px;
    letter-spacing: 5px;
    color: #ff7553;
  }
`;

export const SidebarBlock = styled.div`
  padding: 10px 0;
`;

export const SidebarItem = styled.div`
  position: relative;
  &:before{
    content: "";
    background: transparent;
    height: 100%;
    width: calc(100% - 10px);
    position: absolute;
    left: 0;
    top: 0;
    z-index: 1;
    border-radius: 0 20px 20px 0;
    transition: 0.2s all ease-in-out;
  }
  .sidebar-item-link{
    font-size: 16px;
    font-weight: 500;
    color: #505f79;
    padding: 10px 10px 10px 40px;
    margin: 10px 0;
    display: flex;
    align-items: center;
    position: relative;
    z-index: 2;
    transition: 0.2s all ease-in-out;
    &.has-color{
      .sidebar-icon{
        position: relative;
        &:before{
          content: "";
          position: absolute;
          width: 12px;
          height: 12px;
          border-radius: 4px;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        }
      }
    }
    &.blue{
      .sidebar-icon{
        &:before{
          background: #2684ff;
        }
      } 
      .sidebar-notify{
        background: #2684ff;
      }
    }
    &.yellow{
      .sidebar-icon{
        &:before{
          background: #ffc400;
        }
      } 
      .sidebar-notify{
        background: #ffc400;
      }
    }
    &.violet{
      .sidebar-icon{
        &:before{
          background: #6555c0;
        }
      } 
      .sidebar-notify{
        background: #6555c0;
      }
    }
  }
  .sidebar-icon{
    color: #757575;
    font-size: 25px;
    margin-right: 15px;
    display: flex;
    transition: 0.2s all ease-in-out;
    align-items: center;
    width: 25px;
    height: 25px;
  }
  .sidebar-notify{
    color: #ffffff;
    font-size: 9px;
    font-weight: 300;
    width: 23px;
    height: 23px;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #505f79;
    border-radius: 100%;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 20px;
    letter-spacing: 1px;
  }
  &:hover{
    .sidebar-item-link,
    .sidebar-icon{
      color: #ffffff;
    }
    &:before{
      background: rgba(76, 154, 255, 0.4);
    }
  }
`;
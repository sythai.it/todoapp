import react from 'react';
import { Link } from 'react-router-dom';
import { 
  BiStar, 
  BiAlarm, 
  BiCustomize, 
  BiCalendarCheck, 
  BiCalendarEvent 
} from "react-icons/bi";
import { SiSnapcraft } from "react-icons/si";

import * as S from "./style";


const sidebarListTop = [
  {
    icon: <BiCustomize/>,
    name: "Overview",
    link: "/",
    noticeNumber: 0
  },
  {
    icon: <BiCalendarEvent/>,
    name: "Today",
    link: "/",
    noticeNumber: 0
  },
  {
    icon: <BiStar/>,
    name: "Important",
    link: "/",
    noticeNumber: 3
  },
  {
    icon: <BiAlarm/>,
    name: "Scheduled",
    link: "/",
    noticeNumber: 9
  },
  {
    icon: <BiCalendarCheck/>,
    name: "Done",
    link: "/",
    noticeNumber: 0
  }
]

const sidebarListBottom = [
  {
    color: "blue",
    name: "Design",
    link: "/",
    noticeNumber: 21
  },
  {
    color: "yellow",
    name: "Marketing",
    link: "/",
    noticeNumber: 8
  },
  {
    color: "violet",
    name: "Development",
    link: "/",
    noticeNumber: 37
  }
]

interface Props {
  className?: string;
}

function Sidebar(props: Props){
  const { className } = props;

  const RenderSidebarTop = sidebarListTop && sidebarListTop.map((item, index) => {
    return(
      <S.SidebarItem key={index} className={className}>
        <a href={item.link} className="sidebar-item-link">
          <span className="sidebar-icon">{item.icon}</span>
          <span className="sidebar-name">{item.name}</span>
          {item.noticeNumber > 0 && 
            <span className="sidebar-notify">{item.noticeNumber}</span>
          }
        </a>
      </S.SidebarItem>
    )
  })

  const RenderSidebarBottom = sidebarListBottom && sidebarListBottom.map((item, index) => {
    return(
      <S.SidebarItem key={index} className={className}>
        <a href={item.link} className={`sidebar-item-link has-color ${item.color}`}>
          <span className="sidebar-icon"></span>
          <span className="sidebar-name">{item.name}</span>
          {item.noticeNumber > 0 && 
            <span className="sidebar-notify">{item.noticeNumber}</span>
          }
        </a>
      </S.SidebarItem>
    )
  })

  return(
    <S.Wrapper className="sidebar">
      <S.Logo>
        <span className="logo">
          <SiSnapcraft/>
        </span>
        <span className="logo-name">todoui</span>
      </S.Logo>
      <S.SidebarBlock>
        {RenderSidebarTop}
      </S.SidebarBlock>
      <S.SidebarBlock>
        {RenderSidebarBottom}
      </S.SidebarBlock>
    </S.Wrapper>
  );
}

export default Sidebar;
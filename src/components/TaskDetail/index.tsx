import react from 'react';
import { BiStar } from "react-icons/bi";
import { CgMoreVerticalAlt } from "react-icons/cg";
import { IoIosAdd } from "react-icons/io";

import { CheckButton } from "components";


import * as S from "./style";

interface Props {
  className?: string;
}

function TaskDetail(props: Props){
  const { className } = props;

  return(
    <S.Wrapper className={className}>
      <CheckButton isChecked={true} className="detail-check-btn" />
      <div className="detail-content" >
        <div className="detail-head">
          <span className="detail-head-name">Create styleguide for aseio</span>
          <button className="isfavorite">
            <BiStar/>
          </button>
          <button className="more-function">
            <CgMoreVerticalAlt/>
          </button>
        </div>
        <div className="detail-note">
          <p>Few would argue that, despite the advancements of femisnism over the pasr three decades, women still face adoube standard when it comes to their behavior.</p>
          <p>While men's borderline-inappropriate behavior is often laughed off as "boys will be boys," women face higher conduct standards - especially un the worksplace. That's why it's crucial that, as women, our behavior on the job is beyound reproach.</p>
          <p>Small Towns and Big States.</p>
        </div>
        <div className="detail-steps">
          <div className="add">
            <button className="add-btn">
              <IoIosAdd/>
            </button>
            <span>Add step</span>
          </div>
          <div className="step">
            <CheckButton isChecked={false} className="step-check-btn" />
            <span>started sprint</span>
          </div>
          <div className="step">
            <CheckButton isChecked={false} className="step-check-btn" />
            <span>Get developer's inputs</span>
          </div>
        </div>
      </div>
    </S.Wrapper>
  );
}

export default TaskDetail;
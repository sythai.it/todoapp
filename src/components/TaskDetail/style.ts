import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  .detail-content{
    width: 100%;
    padding: 0 10px;
  }
  .detail-check-btn{
    padding: 0 15px 0 30px;
  }
  .detail-head{
    display: flex;
  }
  .detail-head-name{
    font-size: 20px;
    font-weight: 600;
    color: #505f79;
    width: calc(100% - 120px);
  }
  .isfavorite,
  .more-function{
    font-size: 25px;
    border: none;
    background: transparent;
    color: #757575;
    width: 60px;
    cursor: pointer;
  }
  .detail-note{
    font-size: 15px;
    font-weight: 500;
    color: #505f79;
    padding-right: 130px;
    margin-bottom: 50px;
  }
  .detail-steps{
    .add, .step{
      display: flex;
      align-items: center;
      margin-bottom: 20px;
    }
    .add{
      span{
        color: #63a5fe;
        font-weight: 400;
      }
    }
    .step{
      span{
        color: #858fa1;
        font-weight: 500;
      }
    }
    .add-btn{
      background: transparent;
      font-size: 25px;
      border: none;
      color: #757575;
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
      padding: 0;
      margin-right: 5px;
    }
    .step-check-btn{
      .check-btn{
        width: 20px;
        height: 20px;
        border-color: #757575;
        border-width: 1px;
        margin: 0 8px 0 2px;
      }
    }
  }
`;
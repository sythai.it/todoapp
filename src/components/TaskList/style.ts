import styled from "styled-components";

export const Wrapper = styled.div`
  .task-item{
    display: flex;
    align-items: center;
    padding: 25px 55px 25px 25px;
    position: relative;
    transition: 0.2s all ease-in-out;
    &:not(:last-child){
      border-bottom: rgba(80, 95, 121, 0.1) 1px solid;
    }
    .task-info{
      margin-left: 20px;
    }
    .task-name{
      font-size: 16px;
      font-weight: 500;
      color: #505f79;
      margin-bottom: 10px;
      display: block;
    }
    .task-status{
      color: #757575;
      display: flex;
      align-items: center;
      .task-status-date{
        font-size: 12px;
        font-weight: 500;
      }
      .task-status-icon{
        color: #757575;
      }
      .clock{
        margin-right: 10px;
      }
      .note,
      .attachment,
      .notify{
        margin-left: 25px;
      }
    }
    .task-favorite{
      color: #b7b8b8;
      font-size: 22px;
      position: absolute;
      right: 20px;
      top: 50%;
      transform: translateY(-50%);
      border: none;
      background: transparent;
      cursor: pointer;
      padding: 0;
    }
    &:hover{
      background-color: rgba(255, 255, 255, 0.9);
    }
  }
`;
import react, {useState} from 'react';
import { 
  FaRegBell,
  FaRegClock,
  FaRegStickyNote
} from "react-icons/fa";
import { GrAttachment } from "react-icons/gr";
import { BiStar } from "react-icons/bi";

import { CheckButton } from "components";

import * as S from "./style";

const taskList = [
  {
    isDone: false,
    isFavorite: false,
    isNotify: true,
    isAttached: false,
    isnote: false,
    taskName: "Calendar component iteration",
    date: "Tue, 1 May",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: true,
    isAttached: true,
    isnote: false,
    taskName: "Convertsation tweaks-iphone",
    date: "Tomorrow",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: false,
    taskName: "Design review - Mable Roberts",
    date: "Mon, 21 Apr",
  },
  {
    isDone: true,
    isFavorite: false,
    isNotify: true,
    isAttached: false,
    isnote: true,
    taskName: "Create styleguide for aseio",
    date: "Sun, 3 Mar",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: false,
    taskName: "Interviews - meeting with HR",
    date: "Thu, 2 May",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: true,
    taskName: "Looking For brochure printing Soluti...",
    date: "Tue, 27 Feb",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: true,
    taskName: "How Does An Lcd Screen Work",
    date: "Wed, 1 Feb",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: true,
    taskName: "How Does An Lcd Screen Work",
    date: "Wed, 1 Feb",
  },
  {
    isDone: false,
    isFavorite: false,
    isNotify: false,
    isAttached: false,
    isnote: true,
    taskName: "How Does An Lcd Screen Work",
    date: "Wed, 1 Feb",
  }
]

interface Props {
  className?: string;
  data?: any;
}

function TaskList(props: Props){
  const { className, data } = props;
  const [isChecked, setChecked] = useState(false);
  const handleCheck = (item: any, index: number) => {
    setChecked(!item.isDone)
  }

  const RenderTaskList = taskList && taskList.map((item, index) => {
    return (
      <div key={index} className="task-item">
        <CheckButton 
          isChecked={item.isDone}
          onClick={() => handleCheck(item, index)}
        />
        <div className="task-info">
          <span className="task-name" >{item.taskName}</span>
          <div className="task-status" >
            <FaRegClock className="task-status-icon clock"/>
            <span className="task-status-date">{item.date}</span>
            {item.isnote === true &&
              <FaRegStickyNote className="task-status-icon note"/>
            }
            {item.isAttached === true &&
              <GrAttachment className="task-status-icon attachment"/>
            }
            {item.isNotify === true &&
              <FaRegBell className="task-status-icon notify"/>
            }
          </div>
        </div>
        <button className="task-favorite">
          <BiStar/>
        </button>
      </div>
    )
  })

  return(
    <S.Wrapper className="task-list">
      {RenderTaskList}
    </S.Wrapper>
  );
}

export default TaskList;
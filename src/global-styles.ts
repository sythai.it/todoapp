import { createGlobalStyle } from 'styled-components'
import { Themes } from './configs/theme'

const GlobalStyle = createGlobalStyle`

  html,
  body {
    font-family: 'Montserrat', sans-serif;
    height: 100%;
    width: 100%;
    font-size: 14px;
    font-weight: 300;
    color: #505f79;
  }

  body {
    /* overflow: hidden; */
  }

  body.fontLoaded {
    font-family: 'Rubik', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #root {
    background-color: #eeeeee;
    height: 100%;
  }

  a{
    text-decoration: none;
    &:hover{
      text-decoration: none;
    }
  }

  p,
  label {
    font-family: 'Rubik', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    line-height: 1.5em;
  }
  
  .image {
    max-width: 100%;
    object-fit: cover;
  }

  img{
    display: block;
  }
  
  .ant-form-item-children-icon {
    top: 50% !important;
  }
  .login__logo-block {
    //es
    background-color: ${Themes['default'].containerBg};
  }
  .ant-menu-vertical .ant-menu-item {
    margin: 0 auto;
  }
  .ant-menu-inline .ant-menu-item {
    margin: 0 auto;
  }
  .anticon {
    vertical-align: 0.2em !important;
  }
  tr.ant-table-expanded-row > td {
    background: ${Themes['default'].mainWhite};
  }
  .white-list__modal-title {
    font-size: 2rem;
    font-weight: 600;
    text-align: center;
  }
  .input__label {
    color: ${Themes['default'].inputLabel};
  }
  .ant-layout-sider {
    flex: 0 0 250px !important;
    max-width: 250px !important;
    min-width: 250px !important;
    width: 250px !important;
  }
  .ant-layout-sider-collapsed {
    flex: 0 0 80px !important;
    max-width: 80px !important;
    min-width: 80px !important;
    width: 80px !important;
  }

  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus,
  textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover,
  textarea:-webkit-autofill:focus,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus {
    -webkit-text-fill-color: #505f79;
    -webkit-box-shadow: 0 0 0px 1000px transparent inset;
    transition: background-color 5000s ease-in-out 0s;
  }

  input{
    ::-webkit-input-placeholder {
      color: #505f79;
      font-size: 16px;
    }

    :-moz-placeholder { /* Firefox 18- */
      font-size: 16px;
      color: #505f79;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
      font-size: 16px;
      color: #505f79;  
    }

    :-ms-input-placeholder {  
      font-size: 16px;
      color: #505f79;  
    }
  }
`;

export default GlobalStyle

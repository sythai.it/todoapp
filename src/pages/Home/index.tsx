import react from "react";
import * as S from "./style";
import { Default } from "layouts";
import { Sidebar } from "components";

function Home() {
  return (
    <Default
      content={
        <S.Wrapper>
          <Sidebar></Sidebar>
        </S.Wrapper>
      }
    />
  );
}

export default Home;

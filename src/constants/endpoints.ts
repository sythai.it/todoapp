const Endpoints = {
  DOWNLOAD: '/report',
  LOGIN: '/api/v1/public/users/login',
  LOGIN_TWITTER: '/login-twitter',
  GET_ADMIN_DEPOSIT: '/api/users/page',
  WHITE_LIST_ACTION: '/api/email/whitelisted',
  REJECT_ACTION: '/api/email/rejected',
  GET_WHITELIST: '/api/whitelisted/unregistered',
  GET_NEW_WHITELIST: '/api/whitelisted/query',
  CREATE_WHITELIST_EDM: '/api/whitelisting',
  REJECT_FROM_WHITELIST: '/api/email/rejected/from/whitelisted',
  SEARCH_USER: '/api/user/search',
  DEPOSIT_LIST: '/api/v1/public/transaction/gets_deposit',
  WITHDRAW_LIST: '/api/v1/public/transaction/gets_withdraw'
}

export default Endpoints
const Routes = {
  LOGIN: '/login',
  HOME: '/home',
  REGISTER: '/register',
  ADMIN_DEPOSIT: '/admin/deposit',
  ADMIN_WITHDRAW: '/admin/withdraw',
  WHITE_LIST: '/white-list',
  REGISTERED: '/registered',
  REJECTED: '/rejected',
  DOWNLOAD: '/download',
  Navigation: '/navigation',
}

export default Routes